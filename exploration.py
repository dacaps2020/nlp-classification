import data


RData = data.read_csv()


#Count per category
CatCount = RData.groupby(['Category']).count()
CatCount

#where category = vehicle, filter for rows that include substring "truck"
Vehicle = RData[RData.Category == 'vehicle']
Vehicle[Vehicle['EventDescription'].str.contains("truck", na=False)]
#filter for rows with substring 'truck' for any category
RData[RData['EventDescription'].str.contains("truck", na=False)]
#class distribution for observations which include substring 'truck'
Truck = RData[RData['EventDescription'].str.contains(" truck", na=False)]
Truck.groupby(['Category']).count()

#same for conductor
Conductor = RData[RData['EventDescription'].str.contains(" conductor", na=False)]
Conductor.groupby(['Category']).count()

#
CData = data.read_csv(clean=True)
