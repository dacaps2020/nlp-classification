from copy import deepcopy
from operator import itemgetter
from random import sample
import pandas as pd
import numpy as np
#%matplotlib inline
import matplotlib.pyplot as plt

from matplotlib import style
style.use('ggplot')

import seaborn as sn

from scipy import sparse
import scikitplot.metrics as skplt
import nltk

from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
from nltk.stem import PorterStemmer, WordNetLemmatizer, SnowballStemmer

from nltk.classify.scikitlearn import SklearnClassifier
from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer

from sklearn.neighbors import KNeighborsClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression, SGDClassifier, Perceptron
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.svm import SVC, LinearSVC
#from sklearn.neural_network import MLPClassifier
#from sklearn.ensemble import VotingClassifier

from sklearn.metrics import classification_report, accuracy_score, confusion_matrix, make_scorer, precision_score, recall_score, f1_score, balanced_accuracy_score
from sklearn.pipeline import Pipeline
from sklearn.model_selection import GridSearchCV, train_test_split, cross_val_score, cross_validate, StratifiedKFold

def simple_model_testing(model, x_train, y_train, x_test, y_test):
    #[0]: Model constructor
    #[1]: Model name/description
    text_clf = Pipeline([('vect', CountVectorizer()), ('tfidf', TfidfTransformer()), ('clf', model[0])])
    trained_text_clf = text_clf.fit(x_train, y_train)

    cross_scores = cross_val_score(text_clf, x_train, y_train, cv = 10, scoring = 'balanced_accuracy')

    predicted = trained_text_clf.predict(x_test)

    print(model[1])
    #print('Simple accuracy:', )
    print('Balanced accuracy:', cross_scores.mean())
    #https://scikit-learn.org/stable/modules/generated/sklearn.metrics.classification_report.html
    print(classification_report(y_test, predicted))

def model_optimisation_and_testing(model_list, scoring_strategy, primary_score, x_train, y_train, x_test, y_test, random_state_b = None):
    #[0]: Model constructor
    #[1]: Model name/description
    #[2]: Hyperparameter dictionary
    #[3]: Whether the model has pedict_proba as a method
    
    #[4]: Best optimised and trained classifier model
    #[5]: Predicted results from above model
    #[6]: Labelled predicted results from above model
    #[7]: Testing scores dictionary of lists
    #[9]: Confusion matrix list
    #[11]: Best parameters from above model
    #[12]: y_test used for above model
    
    #https://scikit-learn.org/stable/modules/model_evaluation.html#scoring-parameter
    
    text_clf = Pipeline([('vect', CountVectorizer()), 
                         ('tfidf', TfidfTransformer()), 
                         ('clf', model_list[0])],
                       verbose = 1)
    
    skf = StratifiedKFold(n_splits = 5, shuffle = True, random_state = random_state_b)
    
    hyperparameter_optimisation_text_clf = GridSearchCV(text_clf, 
                                      model_list[2], 
                                      cv = skf,
                                      n_jobs = -1, 
                                      scoring = scoring_strategy, 
                                      refit = primary_score, 
                                      verbose = 2)
    
    inner_fold_trained_text_clf = hyperparameter_optimisation_text_clf.fit(x_train, y_train)
    
    #--Uses the best parameters gridsearchcv found and trains the model on the entire x_train/y_train set
    best_parameters = inner_fold_trained_text_clf.best_params_
    hyperparameter_optimised_text_clf = text_clf.set_params(**best_parameters)
    outer_fold_trained_text_clf = hyperparameter_optimised_text_clf.fit(x_train, y_train)
    
    predicted = outer_fold_trained_text_clf.predict(x_test)

    #print(model_list[1])
    #print()
    #print('Best parameters:', best_parameters)
    #print()
    
    #--Prints the best scores from the gridsearch cross-validation
    #best_index = np.nonzero(trained_text_clf.cv_results_[ f'rank_test_{primary_score}' ] == 1)[0][0]
    #print('Training cross-validation mean scores:')
    #for score_method in [*scoring_strategy]:
    #    print('\t' + f'{score_method:<20}  {trained_text_clf.cv_results_[ f"mean_test_{score_method}" ][best_index]:>20}')
    
    #--Checks to see if model_list[7] dictionary of lists has been initialised (in case this function wasn't called through cross_validation)
    if model_list[7] is None:
        model_list[7] = {}
        for score_method in [*scoring_strategy]:
            model_list[7][score_method] = []
    
    #print()
    #print('Testing scores:')
    for score_method in [*scoring_strategy]:
        #print('\t', score_method, scoring_strategy[score_method](trained_text_clf, x_test, y_test))
        score = scoring_strategy[score_method](outer_fold_trained_text_clf, x_test, y_test)
        model_list[7][score_method].append(score)
        
        #--Prints the primary score in bold
        #if score_method == primary_score:
        #    print('\33[1m' + '\t' + f'{score_method:<20}  {score:>20}' + '\33[0m')
        #else:
        #    print('\t' + f'{score_method:<20}  {score:>20}')
    
    #print()
    #print(classification_report(y_test, predicted)) #makes a space after
    
    #--Checks to see if model_list[9] list has been initialised (in case this function wasn't called through cross_validation)
    if model_list[9] is None:
        model_list[9] = []
    
    model_list[9].append(confusion_matrix(y_test, predicted))
    
    #--Displaying the confusion matrix
    #confusion_matrix = skplt.plot_confusion_matrix(y_test, predicted)
    #confusion_matrix.tick_params(axis = 'x', rotation = 80)
    #confusion_matrix.figure.set_facecolor('white')
    #display(confusion_matrix.figure)
    
    #--If the new primary_score is greater than or equal to the maximum currently listed then store the new model and its results as the best
    if model_list[7][primary_score][-1] >= max(model_list[7][primary_score]):
        model_list[4] = deepcopy(outer_fold_trained_text_clf)
        model_list[5] = predicted
        model_list[11] = best_parameters
        model_list[12] = y_test
    
        #if model_list[3]:
        try:
            #--Probabilities are for each category (given in the order of .classes_)
            predicted_probabilities = outer_fold_trained_text_clf.predict_proba(x_test)
            labelled_predicted_probabilities = []

            for i in range(0, len(predicted_probabilities)):
                labelled_predicted_probabilities.append(list(zip(outer_fold_trained_text_clf.classes_, predicted_probabilities[i])))
            
            model_list[6] = labelled_predicted_probabilities
        except AttributeError:
            pass
    
    print('------------------------------------------------------------------------------------------------------------------')
    print('------------------------------------------------------------------------------------------------------------------')
    
    
def category_prediction_analysis(model_list, category):
    #--This uses the list indexes, not the indexes of the original dataset, in order to access the prediction results (which don't keep the index except through x_test)
    category_list_indexes = [ i for i in range(len(model_list[12])) if model_list[12].values[i] == category ]
    #truncated_category_indexes = category_indexes[0:20]
    
    try:
        random_category_list_indexes = sample(category_list_indexes, 20)
    except ValueError:
        random_category_list_indexes = category_list_indexes
    
    print(model_list[1])
    print()
    print('Actual category:', category)
    print('Predicted category:')
    
    #if model_list[3]:
    try:
        for index in random_category_list_indexes:
            labelled_nonzero_probabilities = [ model_list[6][index][j] for j in range(len(model_list[6][index])) if model_list[6][index][j][1] != 0 ]
            sorted_labelled_nonzero_probabilities = sorted(labelled_nonzero_probabilities, key = itemgetter(1), reverse = True)
            
            print('\t', model_list[5][index])
            
            for labelled_nonzero_probability in sorted_labelled_nonzero_probabilities:
                if labelled_nonzero_probability[0] == category:
                    print('\033[1m' + '\t\t' + f'{labelled_nonzero_probability[0]:<15}  {labelled_nonzero_probability[1]:>24}' + '\033[0m')
                else:
                    print('\t\t' + f'{labelled_nonzero_probability[0]:<15}  {labelled_nonzero_probability[1]:>24}')
            
            print()
    
    #else:
    except TypeError:
        for index in random_category_list_indexes:
            print('\t', model_list[5][index])
    
    print('------------------------------------------------------------------------------------------------------------------')
    print('------------------------------------------------------------------------------------------------------------------')
    
def cross_validation(model_list, x_input_name, random_state_a = None, random_state_b = None):
    print('------------------------------------------------------------------------------------------------------------------')
    print('------------------------------------------------------------------------------------------------------------------')
    #[0]: Model constructor
    #[1]: Model name/description
    #[2]: Hyperparameter dictionary
    #[3]: Whether the model has pedict_proba as a method
    #[4]: Best optimised and trained classifier model
    #[5]: Predicted results from above model
    #[6]: Labelled predicted results from above model
    #[7]: Testing scores dictionary of lists
    #[9]: Confusion matrix list
    #[11]: Best parameters from above model
    #[12]: y_test used for above model
    
    #[8]: Testing scores dictionary of averages
    #[10]: Confusion matrix average
    
    
    scoring_strategy = {'balanced_accuracy': make_scorer(balanced_accuracy_score),
                        'accuracy': make_scorer(accuracy_score),
                        'precision_weighted': make_scorer(precision_score, average = 'weighted'),
                        'recall_weighted': make_scorer(recall_score, average = 'weighted'),
                        'f1_weighted': make_scorer(f1_score, average = 'weighted')}
    
    primary_score = 'balanced_accuracy'
    
    model_list[7] = {}
    for score_method in [*scoring_strategy]:
        model_list[7][score_method] = []
    
    model_list[8] = {}
    
    model_list[9] = []
    
    skf = StratifiedKFold(n_splits = 5, shuffle = True, random_state = random_state_a)
    
    for train_index, test_index in skf.split(df[x_input_name], df['Category']):
        #print("TRAIN:", train_index, len(train_index))
        #print("TEST:", test_index, len(test_index))
        x_train, x_test = df[x_input_name][train_index], df[x_input_name][test_index]
        y_train, y_test = df['Category'][train_index], df['Category'][test_index]
        model_optimisation_and_testing(model_list, scoring_strategy, primary_score, x_train, y_train, x_test, y_test, random_state_b)
    
    #--Prints the model description
    print()
    print('\033[1m' + model_list[1] + '\033[0m')
    
    #--Prints the best parameters found:
    print()
    print('Best parameters found:')
    print(model_list[11])
    
    #--Mean cross-validation scores
    print()
    print('Testing cross-validation mean scores:')
    for score_method in [*scoring_strategy]:
        mean_score = np.mean(model_list[7][score_method])
        model_list[8][score_method] = mean_score
        
        #--Prints the primary score in bold
        if score_method == primary_score:
            print('\33[1m' + '\t' + f'{score_method:<20}  {mean_score:>20}' + '\33[0m')
        else:
            print('\t' + f'{score_method:<20}  {mean_score:>20}')
    
    #--Prints best classification report
    print()
    print('Best classification report:')
    print(classification_report(model_list[12], model_list[5]))
    
    #--Mean confusion matrix
    mean_confusion_matrix = np.mean(model_list[9], axis = 0)
    model_list[10] = mean_confusion_matrix
    
    categories = model_list[4].classes_
    df_cm = pd.DataFrame(mean_confusion_matrix, index = [i for i in categories], columns = [i for i in categories])
    #plt.figure(figsize = (20,7))
    mean_confusion_matrix_heatmap = sn.heatmap(df_cm, annot = True, linewidths=.5, fmt = '.1f')
    mean_confusion_matrix_heatmap.figure.set_figheight(7)
    mean_confusion_matrix_heatmap.figure.set_figwidth(20)
    mean_confusion_matrix_heatmap.set_xticklabels(mean_confusion_matrix_heatmap.get_xticklabels(), rotation = 0)
    mean_confusion_matrix_heatmap.set_title(model_list[1], fontsize = 31)
    display(mean_confusion_matrix_heatmap.figure)
    mean_confusion_matrix_heatmap.figure.clf()
    

#[i][0]: Model constructor
#[i][1]: Model name/description
default_models = [
    (KNeighborsClassifier(),
         "K-Nearest Neighbors"),
    (DecisionTreeClassifier(),
         "Decision Tree"),
    (RandomForestClassifier(),
         "Random Forest"),
    (LogisticRegression(),
         "Logistic Regression"),
    (SGDClassifier(),
         "SGD Classifier"),
    (MultinomialNB(),
         "Naive Bayes"),
    (BernoulliNB(),
         "Bernoilli"),
    (SVC(kernel = 'linear'),
         "SVC - linear"),
    (Perceptron(),
         "Perceptron")#,
    #{MLPClassifier(),
        #"MLPClassifier"}
]

'''
Can use
    'vec__max_df': (0.5, 0.625, 0.75, 0.875, 1.0),  
    'vec__max_features': (None, 5000, 10000, 20000),  
    'vec__min_df': (1, 5, 10, 20, 50),  
    'tfidf__use_idf': (True, False),  
    'tfidf__sublinear_tf': (True, False),  
    'vec__binary': (True, False),  
    'tfidf__norm': ('l1', 'l2'),  
    'clf__alpha': (1, 0.1, 0.01, 0.001, 0.0001, 0.00001)
to change parameters of vectorisor and tfidf transformer
vec__, tfidft__
'''

#Greyed-out parameters indicate testing was done and we've narrowed the options

#[i][0]: Model constructor
#[i][1]: Model name/description
#[i][2]: Hyperparameter dictionary
#[i][3]: Whether the model has pedict_proba as a method
models_with_hyperparameter_sets = [
    [#https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html
        KNeighborsClassifier(),
        "K-Nearest Neighbors",
        {
            'clf__n_neighbors': list(range(1, 11)),
            'clf__weights': ['distance'],#['uniform', 'distance'],
            'clf__leaf_size': [25, 30, 35]
        },
        True
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.tree.DecisionTreeClassifier.html
        DecisionTreeClassifier(),
        "Decision Tree",
        {
            'clf__criterion':['gini'],#['gini','entropy'], 
            'clf__max_depth':[None, 5, 10, 15, 20, 25, 30, 35, 40]#[None, 4, 5, 6, 7, 8, 9, 10, 11, 12, 15, 20, 30, 40, 50, 70, 90, 120, 150]
        },
        True
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.RandomForestClassifier.html
        RandomForestClassifier(),
        "Random Forest",
        {
            'clf__n_estimators': [100, 200, 700],
            'clf__max_features': ['sqrt'],#['auto', 'sqrt', 'log2'],
            'clf__max_depth': [None]#[None, 10, 20, 50, 100]
        },
        True
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html
        LogisticRegression(),
        "Logistic Regression",
        {
            'clf__C': [10]#[0.001, 0.01, 0.1, 1, 10, 100, 1000]
        },
        True
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDClassifier.html
        SGDClassifier(loss = 'hinge'),
        "SGD Classifier - hinge",
        {
            'clf__max_iter': [None, 5, 10, 20, 50, 1000],
            'clf__alpha': [0.0001],#[0.001, 0.0001, 0.00001, 0.000001],
            'clf__penalty': ['elasticnet'],#['l2', 'elasticnet'],
            'clf__l1_ratio':[0.2]#[0.05, 0.1, 0.15, 0.2]
        },
        False
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.SGDClassifier.html
        SGDClassifier(loss = 'modified_huber'),
        "SGD Classifier - modified_huber",
        {
            'clf__max_iter': [None, 5, 10, 20, 50, 1000],
            'clf__alpha': [0.0001],#[0.001, 0.0001, 0.00001, 0.000001],
            'clf__penalty': ['elasticnet'],#['l2', 'elasticnet'],
            'clf__l1_ratio':[0.2]#[0.05, 0.1, 0.15, 0.2]
        },
        True
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.MultinomialNB.html
        MultinomialNB(),
        "Naive Bayes",
        {
            'clf__alpha': [0.01]#[1, 0.1, 0.01, 0.001, 0.0001, 0.00001, 0]
        },
        True
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.naive_bayes.BernoulliNB.html
        BernoulliNB(),
        "Bernoilli",
        {
            'clf__alpha' : [0.01]#[0, 0.0001, 0.001, 0.01, 0.1, 0.5, 1, 2, 10]
        },
        True
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
        SVC(kernel = 'linear', probability = True),
        "SVC - linear",
        {
            'clf__C': [10],#[0.001, 0.01, 0.1, 1, 10],
            'clf__gamma': [0.1]#['scale', 0.001, 0.01, 0.1, 1]
        },
        True
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
        SVC(kernel = 'rbf', probability = True),
        "SVC - rbf",
        {
            'clf__C': [10],#[0.001, 0.01, 0.1, 1, 10],
            'clf__gamma': [0.1]#['scale', 0.001, 0.01, 0.1, 1]
        },
        True
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.svm.LinearSVC.html
        LinearSVC(),
        "LinearSVC",
        {
            'clf__C': [1]#[0.001, 0.01, 0.1, 1, 10]
        },
        False
    ],
    [#https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Perceptron.html
        Perceptron(),
        "Perceptron",
        {
            'clf__alpha': [0.001]#[0.001, 0.0001, 0.00001, 0.000001]
        },
        False
    ]
]

for model_list in models_with_hyperparameter_sets:
    model_list += 9 * [None]
