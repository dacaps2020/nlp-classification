from definitions import (
	ROOT_DIR,
	DATA_DIR,
	contraction_mapping
	)
import numpy as np  
import pandas as pd 
import re           
from bs4 import BeautifulSoup 
from nltk.corpus import stopwords   
import warnings
pd.set_option("display.max_colwidth", 200)
warnings.filterwarnings("ignore")



#function to return raw data with trimmed columns
#[groundfire = true] to include groundfire, [head=n] to return only n amount of rows, otherwise empty to return all
#[clean = true] to read clean version
def read_csv(clean=False,groundfire=False,head=15016):
	address = (DATA_DIR + '/incidents_2010_2019.csv') if not clean else (DATA_DIR + '/cleaned_data.csv')
	RData = pd.read_csv(address,encoding='Latin-1', sep=',', nrows=head)
	
	if clean:
		return RData #layered if statement because cleaned_data already has trimmed columns
	
	if not clean:
		if not groundfire: 
			RData = RData.drop(['IncidentNumber', 'NetworkType', 'Address', 'IncidentFireSeverity', 'Lat', 'Long', 'Postcode', 'GroundFire'], axis=1)
		if groundfire:
			RData = RData.drop(['IncidentNumber', 'NetworkType', 'Address', 'IncidentFireSeverity', 'Lat', 'Long', 'Postcode'], axis=1)
	
		return RData
	
#textcleaning
stop_words = set(stopwords.words('english'))

#function to clean text of a single row
#borrowed from: https://gist.github.com/aravindpai/c8364edf7c2d5083c97de71607d4bb25#file-textcleaning-py
def text_cleaner(text):
    newString = text.lower()
    newString = BeautifulSoup(newString, "lxml").text
    newString = re.sub(r'\([^)]*\)', '', newString)
    newString = re.sub('"','', newString)
    newString = ' '.join([contraction_mapping[t] if t in contraction_mapping else t for t in newString.split(" ")])    
    newString = re.sub(r"'s\b","",newString)
    newString = re.sub("[^a-zA-Z]", " ", newString) 
    tokens = [w for w in newString.split() if not w in stop_words]
    long_words=[]
    for i in tokens:
        if len(i)>=3:                  #removing short word
            long_words.append(i)   
    return (" ".join(long_words)).strip()
	

#argument dirty is variable name of the pandas dataframe
def apply_cleaner(dirty):
	cleaned_incident = []
	cleaned_description = []
	
	dirty = dirty.replace(np.nan, '', regex=True) #fixes bug where function tried to turn NAN values into lower case and fails
	
	for t in dirty['IncidentCause']:
		cleaned_incident.append(text_cleaner(t))
	for t in dirty['EventDescription']:
		cleaned_description.append(text_cleaner(t))
	
	dirty['cleaned_incident'] = cleaned_incident
	dirty['cleaned_description'] = cleaned_description
	
	dirty["Category"] = dirty["Category"].str.lower()
	dirty = dirty[dirty.Category != '']
	dirty = dirty[dirty.Category != 'not reportable - delete']
	dirty = dirty[dirty.Category != 'N/A']
	dirty = dirty.reset_index()
	
	return dirty